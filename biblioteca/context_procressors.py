def mi_primer_context_processor(request):
    """
    Devuelve una lista con los nombres de quienes estan aprendiendo django
    """
    
    return {
        'personas': ['Luisa', 'Nathaly', 'Jorge']
    }
