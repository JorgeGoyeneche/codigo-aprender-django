from django.contrib import admin

# Register your models here.
from biblioteca.models import Autor, Libro


class LibroStackedInline(admin.StackedInline):
    model = Libro
    extra = 0


class LibroTabularInline(admin.TabularInline):
    model = Libro
    extra = 0


@admin.register(Autor)
class AutorAdmin(admin.ModelAdmin):
    list_display = ('nombre_completo', 'email',)
    list_display_links = ('nombre_completo', 'email',)
    search_fields = ('nombre_completo', 'email',)
    readonly_fields = ('nombre_email_juntos',)
    inlines = [LibroStackedInline]
    
    def nombre_email_juntos(self, obj):
        return "{} {}".format(obj.nombre_completo, obj.email)
        
    nombre_email_juntos.short_description = u"Nombre y correo electrónico " \
                                            u"juntos"
    


@admin.register(Libro)
class LibroAdmin(admin.ModelAdmin):
    pass
