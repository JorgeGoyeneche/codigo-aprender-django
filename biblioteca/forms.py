import re

from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.forms import widgets

from biblioteca.models import Autor


class AutorModelForm(forms.ModelForm):
    class Meta:
        model = Autor
        fields = ('nombre_completo', 'email',)
        labels = {
            'nombre_completo': "Nombre del autor",
            'email': u'Correo electrónico'
        }
        # widgets = {
        #     'nombre_completo': widgets.TextInput(
        #         attrs={'placeholder': 'Pepito Perez'}),
        # }
    
    def __init__(self, *args, **kwargs):
        super(AutorModelForm, self).__init__(*args, **kwargs)
        
        self.fields['nombre_completo'].widget.attrs.update({
            'placeholder': 'Pepito Perez',
        })
        self.fields['email'].widget.attrs.update({
            'placeholder': 'correo@ejemplo.com',
        })


class LoginAManoForm(forms.Form):
    username = forms.CharField(max_length=200, required=True)
    password = forms.CharField(max_length=255, required=True,
                               widget=forms.PasswordInput)
    request = None
    
    def clean_username(self):
        username = self.cleaned_data.get('username')
        
        if not User.objects.filter(username=username).exists():
            raise forms.ValidationError('No existe un usuario con este nombre')
        elif not User.objects.filter(username=username).exists():
            user = User.objects.get(username=username)
            if not user.is_active:
                raise forms.ValidationError('Tu usuario no se encuentra '
                                            'activo actualmente')
        return username
    
    def clean_password(self):
        password = self.cleaned_data.get('password')
        username = self.cleaned_data.get('username')
        
        user = authenticate(username=username, password=password)
        
        if user is None:
            raise forms.ValidationError('Tu contraseña es incorrecta')
        
        self.cache_user = user
        return password
    
    def get_user(self):
        return self.cache_user
    
    def __init__(self, request, *args, **kwargs):
        self.request = request
        super(LoginAManoForm, self).__init__(*args, **kwargs)
