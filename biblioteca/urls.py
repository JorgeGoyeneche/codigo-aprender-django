from django.contrib.auth.views import LogoutView
from django.urls import path

from .views import mostrarHolaMundo, MiPrimeraVistaView, AutorCreateView, \
    AutorListView, AutorUpdateView, autorDelete, LibroListView, \
    LibroCreateView, LibroUpdateView, LibroDeleteView, CustomLoginView, \
    LoginAManoView

urlpatterns = [
    path(
        'prueba/',
        mostrarHolaMundo,
        name="prueba"
    ),
    path(
        'mi-primera-vista/',
        MiPrimeraVistaView.as_view(),
        name="mi_primera_vista"
    ),
    path(
        'autor_a_mano/crear/',
        AutorCreateView.as_view(),
        name='autor_crear'
    ),
    path(
        'autor_a_mano/',
        AutorListView.as_view(),
        name='autor_lista'
    ),
    path(
        'autor_a_mano/<int:pk>/',
        AutorUpdateView.as_view(),
        name='autor_editar'
    ),
    path(
        'autor_a_mano/<int:pk>/eliminar/',
        autorDelete,
        name='autor_eliminar'
    ),
    path(
        'libro/',
        LibroListView.as_view(),
        name="libro_lista"
    ),
    path(
        'libro/crear/',
        LibroCreateView.as_view(),
        name="libro_crear"
    ),
    path(
        'libro/<int:pk>/',
        LibroUpdateView.as_view(),
        name="libro_editar"
    ),
    path(
        'libro/<int:pk>/eliminar/',
        LibroDeleteView.as_view(),
        name="libro_eliminar"
    ),
    path(
        'iniciar-sesion/',
        CustomLoginView.as_view(),
        name="login"
    ),
    path(
        'iniciar-sesion-a-mano/',
        LoginAManoView.as_view(),
        name="login_a_mano"
    ),
    path(
        'salir/',
        LogoutView.as_view(),
        name="salir"
    ),
]
