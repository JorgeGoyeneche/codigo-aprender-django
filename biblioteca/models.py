from django.db import models


# Create your models here.
class Autor(models.Model):
    nombre_completo = models.CharField(max_length=500)
    email = models.EmailField()
    
    def __str__(self):
        return self.nombre_completo
    
    class Meta:
        verbose_name = u"Autor"
        verbose_name_plural = u"Autores"


class Libro(models.Model):
    titulo = models.CharField(max_length=250)
    editorial = models.CharField(max_length=100)
    fecha_publicacion = models.DateTimeField(
        verbose_name=u"Fecha de publicación")
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.titulo
    
    class Meta:
        verbose_name = "Libro"
        verbose_name_plural = "Libros"
