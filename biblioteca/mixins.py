from django.contrib import messages
from django.contrib.auth.mixins import AccessMixin
from django.shortcuts import redirect


class ProhibidoJorgeMixin(AccessMixin):
    """Verificar que el usuario no se llame jorge"""
    
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        else:
            user = request.user
            if user.username == 'jorge':
                messages.info(request, u"Jorge, no puedes crear libros")
                return redirect('libro_lista')
            else:
                return super().dispatch(request, *args, **kwargs)
