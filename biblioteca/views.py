from django.contrib import messages
from django.contrib.auth import login, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.forms import widgets
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, UpdateView, ListView, \
    CreateView, DeleteView

from biblioteca.forms import AutorModelForm, LoginAManoForm
from biblioteca.mixins import ProhibidoJorgeMixin
from biblioteca.models import Autor, Libro


@csrf_exempt
def mostrarHolaMundo(request):
    nombre = request.GET.get('nombre', 'invitado')
    
    context = {}
    
    context['mi_lista'] = [1, 2, 3, 4]
    
    context['nombre'] = nombre
    return TemplateResponse(request=request,
                            template='mostrar_hola_mundo.html',
                            context=context)


class MiPrimeraVistaView(TemplateView):
    template_name = 'mostrar_hola_mundo.html'
    
    def get_context_data(self, **kwargs):
        context = super(MiPrimeraVistaView, self).get_context_data(**kwargs)
        context['nombre'] = self.request.GET.get('nombre', 'invitado')
        context['mi_lista'] = [1, 2, 3, 4]
        
        return context


class AutorCreateView(TemplateView):
    template_name = 'autor_a_mano/crear.html'
    
    def get_context_data(self, **kwargs):
        context = super(AutorCreateView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = AutorModelForm()
        return context
    
    def post(self, request, *args, **kwargs):
        form = AutorModelForm(data=request.POST)
        if form.is_valid():
            form.save()
            
            messages.success(request, u"Tu autor ha sido creado")
            return HttpResponseRedirect(
                reverse('autor_lista')
            )
        else:
            return self.render_to_response(
                self.get_context_data(
                    form=form
                )
            )


class AutorListView(TemplateView):
    template_name = 'autor_a_mano/lista.html'
    
    def get_context_data(self, **kwargs):
        context = super(AutorListView, self).get_context_data(**kwargs)
        
        # Atributos que llegan via get
        pagina = self.request.GET.get('page', 1)
        busqueda = self.request.GET.get('busqueda', '')
        
        # queryset de autores
        if busqueda:
            autores = Autor.objects.filter(
                nombre_completo__icontains=busqueda).order_by(
                'nombre_completo')
        else:
            autores = Autor.objects.all().order_by('nombre_completo')
        
        # logica del paginador
        paginador = Paginator(autores, 2)
        try:
            context['autores'] = paginador.page(pagina)
        except EmptyPage:
            context['autores'] = paginador.page(1)
            pagina = 1
        except PageNotAnInteger:
            context['autores'] = paginador.page(1)
            pagina = 1
        
        # contenido agregado al contexto
        context['paginador'] = paginador
        context['pagina_actual'] = int(pagina)
        context['busqueda'] = busqueda
        
        return context


class AutorUpdateView(TemplateView):
    template_name = 'autor_a_mano/editar.html'
    
    def get_context_data(self, **kwargs):
        context = super(AutorUpdateView, self).get_context_data(**kwargs)
        autor = get_object_or_404(Autor, id=self.kwargs['pk'])
        context['autor'] = autor
        if 'form' not in context:
            context['form'] = AutorModelForm(instance=autor)
        return context
    
    def post(self, request, *args, **kwargs):
        autor = get_object_or_404(Autor, id=self.kwargs['pk'])
        form = AutorModelForm(data=request.POST, instance=autor)
        if form.is_valid():
            form.save()
            
            messages.success(request, u"Tu autor ha sido actualizado")
            return HttpResponseRedirect(
                reverse('autor_lista')
            )
        else:
            return self.render_to_response(
                self.get_context_data(
                    form=form
                )
            )


def autorDelete(request, pk):
    if request.method == 'POST':
        autor = get_object_or_404(Autor, id=pk)
        nombre = autor.nombre_completo
        autor.delete()
        
        messages.success(request, u"Tu autor ha sido eliminado correctamente.")
        return HttpResponseRedirect(
            reverse('autor_lista')
        )
    else:
        
        return HttpResponseRedirect(
            reverse('autor_lista')
        )


class LibroListView(ListView):
    model = Libro
    template_name = 'libro/lista.html'
    paginate_by = 2
    queryset = Libro.objects.all()
    
    def get_context_data(self, **kwargs):
        context = super(LibroListView, self).get_context_data(**kwargs)
        context['busqueda'] = self.request.GET.get('busqueda', '')
        
        return context
    
    def get_queryset(self):
        qs = super(LibroListView, self).get_queryset()
        
        busqueda = self.request.GET.get('busqueda', '')
        if busqueda:
            qs = qs.filter(titulo__icontains=busqueda)
        
        return qs


class LibroCreateView(ProhibidoJorgeMixin, CreateView):
    model = Libro
    template_name = 'libro/crear.html'
    queryset = Libro.objects.all().order_by('id')
    fields = '__all__'
    success_url = reverse_lazy('libro_lista')
    
    def get_form(self, form_class=None):
        form = super(LibroCreateView, self).get_form(form_class)
        for name_field in form.fields:
            form.fields[name_field].widget.attrs[
                'style'] = 'background: #E6E6E6;'
        
        return form
    
    def form_valid(self, form):
        messages.success(self.request, u"El libro ha sido creado exitosamente")
        return super(LibroCreateView, self).form_valid(form)


class LibroUpdateView(UpdateView):
    model = Libro
    template_name = 'libro/editar.html'
    queryset = Libro.objects.all().order_by('id')
    fields = '__all__'
    success_url = reverse_lazy('libro_lista')
    
    def get_form(self, form_class=None):
        form = super(LibroUpdateView, self).get_form(form_class)
        for name_field in form.fields:
            form.fields[name_field].widget.attrs[
                'style'] = 'background: #E6E6E6;'
        
        return form
    
    def form_valid(self, form):
        messages.success(self.request,
                         u"El libro ha sido actualizado exitosamente")
        return super(LibroUpdateView, self).form_valid(form)


class LibroDeleteView(DeleteView):
    model = Libro
    queryset = Libro.objects.all().order_by('id')
    success_url = reverse_lazy('libro_lista')
    
    def delete(self, request, *args, **kwargs):
        messages.success(self.request,
                         u"El libro ha sido eliminado exitosamente")
        return super(LibroDeleteView, self).delete(request, *args, **kwargs)


# class LoginTemplateView(TemplateView):
#     template_name = 'iniciar_sesion.html'
#
#     def form_invalid(self, **kwargs):
#         return self.render_to_response(self.get_context_data(**kwargs),
#                                        status=400)
#
#     def post(self, request, *args, **kwargs):
#         username = request.POST.get("username", "")
#         password = request.POST.get("password", "")
#
#         if username != "" and password != "":
#             if User.objects.filter(username=username).exists():
#                 user = User.objects.get(username=username)
#                 user = authenticate(username=user.username,
#                                     password=password)
#                 if user is not None:
#                     login(request, user)
#
#                     return redirect()
#                 else:
#                     if not user.is_active:
#                         messages.info(request,
#                                       "Tu cuenta se encuentra "
#                                       "deshabilitada")
#                     else:
#                         messages.error(request,
#                                        u"Contraseña incorrecta")
#                     return redirect('login')
#             else:
#                 messages.error(request,
#                                u"No existe una cuenta con este correo "
#                                u"electrónico")
#                 return redirect("login")
#         else:
#             messages.error(request,
#                            u"Debes llenar los campos usuario y contraseña")
#             return redirect("login")

class CustomLoginView(LoginView):
    template_name = 'autenticacion/login.html'
    
    def get_success_url(self):
        return reverse('libro_lista')


class LoginAManoView(LoginView):
    template_name = 'autenticacion/login.html'
    form_class = LoginAManoForm
    success_url = reverse_lazy('libro_lista')
    
    def form_valid(self, form):
        """Security check complete. Log the user in."""
        login(self.request, form.get_user())
        return redirect('libro_lista')
